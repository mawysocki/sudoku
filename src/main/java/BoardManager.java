public class BoardManager {
    public int[][] sudoku;
    private Logic logic;

    public BoardManager(Logic logic, int[][] sudoku) {
        this.sudoku = sudoku;
        this.logic = logic;
    }

    public void runAlgorythms(int number) {
        for (int version = 1; version <= 3; version++) {
            logic.resetBoard();
            logic.findEmptyOnBoard(sudoku);
//            logic.checkSumControl();
            if (logic.numberCounter[number-1] < 9) {
                elminateNumber(number, version);
            }
            else {
                System.out.println("Number " + number + " is finished");
                break;
            }
        }

//
//        logic.resetBoard();
//        logic.findEmptyOnBoard(sudoku);
//        elminateNumber(1, 2);


    }

    private void elminateNumber(int number, int version) {
        logic.blockNumber(sudoku, number);
        System.out.println("CURRENT NUMBER: " + number + " VERSION: " + version);
        switch (version) {
            case 1:
                logic.checkSquares();
                break;
            case 2:
                logic.checkColumn();
                break;
            case 3:
                logic.checkRow();
                break;
            default:
                break;
        }

        if (logic.willBeAdded) {
            replaceEmpty(logic.logicBoard, number);
            logic.willBeAdded = false;
        }
//      else {
//            int next_number = number + 1;
//            System.out.println("Cannot add more " + number + " by version " + version);
//            if (next_number < 10) {
//                runAlgorythms(next_number);
//            }
//        }
    }

    public void printBoard() {
        System.out.println("\n");
        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                Logic.operations++;
                System.out.print(sudoku[x][y] + " ");
                if (x == 2 || x == 5) {
                    System.out.print("| ");
                }
            }
            System.out.println("\n");
            if (y == 2 || y == 5) {
                displayFloor();
            }
        }
        System.out.println();
        displayNumberCount();
    }
    public void displayNumberCount() {
        for (int i = 0; i < 9; i++) {
            Logic.operations++;
            System.out.println((i+1) + ")" + logic.numberCounter[i]);
        }
    }

    private void displayFloor() {
        System.out.println("______ _______ ______\n");
    }

    public void replaceEmpty(boolean[][] arr, int number) {
        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                Logic.operations++;
                if (arr[x][y]) {
                    sudoku[x][y] = number;
                }
            }
        }
    }
}
