import javax.swing.*;
import java.awt.*;

public class BoardFrame extends JFrame {


    public final Dimension SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();

    BoardGame panel;
    public BoardFrame() {
        this.setSize(SCREEN_SIZE);
        panel = new BoardGame();
        this.add(panel);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

}
