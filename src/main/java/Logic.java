import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Logic {
    public boolean[][] logicBoard;
    public boolean willBeAdded = false;
    public static int operations = 0;
    public int[] numberCounter = new int[9];
    public int checkSum = 0;

    public Logic() {
        resetBoard();
    }

    public void resetBoard() {
        logicBoard = new boolean[9][9];
    }

    public boolean[][] findEmptyOnBoard(int[][] sudoku) {
        this.numberCounter = new int[9];
        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9;x++) {
                Logic.operations++;
                if (sudoku[x][y] == 0) {
                    logicBoard[x][y] = true;
                } else {
                    this.numberCounter[sudoku[x][y] - 1] += 1;
                }
            }
        }
        return logicBoard;
    }

    public boolean checkSumControl() {
        checkSum = 0;
        for (int x : this.numberCounter) {
            Logic.operations++;
            checkSum += x;
        }
        System.out.println("Current sum: " + checkSum);
        return checkSum == 81;
    }

    public boolean[][] blockNumber(int[][] sudoku, int number) {
        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                if (sudoku[x][y] == number) {
                    blockByColumn(y);
                    blockByRow(x);
                    blockBySquare(x, y);
                }
            }
        }
        return logicBoard;
    }

    public boolean[][] blockByColumn(int y) {
        for (int x = 0; x < 9; x++) {
            logicBoard[x][y] = false;
            Logic.operations++;
        }
        return logicBoard;
    }

    public boolean[][] blockByRow(int x) {
        for (int y = 0; y < 9; y++) {
            logicBoard[x][y] = false;
            Logic.operations++;
        }
        return logicBoard;
    }

    public boolean[][] blockBySquare(int x, int y) {
//        System.out.println("Found number in: " + x + " x " + y);
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                int k = 3 * (x / 3) + i;
                int l = 3 * (y / 3) + j;
//                System.out.println(k + " x " + l);
                logicBoard[k][l] = false;
                Logic.operations++;

            }
        }
        return logicBoard;
    }

    public boolean[][] checkSquares() {
        for (int j = 0; j < 3; j++) {
            for (int i = 0; i < 3; i++) {
                int count = 0;
                for (int k = 0; k < 3; k++) {
                    for (int l = 0; l < 3; l++) {
                        int x = 3 * i + k;
                        int y = 3 * j + l;
                        Logic.operations++;
                        if (logicBoard[x][y]) {
//                            System.out.println(x + "x" + y);
                            count++;
                        }

                    }
                }
//                System.out.println("Count in square: " + count);
                if (count > 1) {
//                    System.out.println("More than one in square: " + i + "x" + j);
                    for (int l = 0; l < 3; l++) {
                        for (int k = 0; k < 3; k++) {
                            int x = 3 * i + k;
                            int y = 3 * j + l;
                            Logic.operations++;
                            if (logicBoard[x][y]) {
                                logicBoard[x][y] = false;
                            }

                        }
                    }
                }
                if (count == 1) {
                    System.out.println("Will be added for square: " + i + "x" + j);
                    willBeAdded = true;
                }
            }
        }
        return logicBoard;
    }

    public boolean[][] checkColumn() {

        for (int y = 0; y < 9; y++) {
            int count = 0;
            for (int x = 0; x < 9; x++) {
                Logic.operations++;
                if (logicBoard[x][y]) {
                    count++;
                }
            }
            if (count == 1) {
                System.out.println("Will be added for column: " + y);
                willBeAdded = true;
            }
            if (count > 1) {
                for (int x = 0; x < 9; x++) {
                    Logic.operations++;
                    if (logicBoard[x][y]) {
                        logicBoard[x][y] = false;
                    }
                }
            }
        }
        return logicBoard;
    }

    public boolean[][] checkRow() {

        for (int x = 0; x < 9; x++) {
            int count = 0;
            for (int y = 0; y < 9; y++) {
                Logic.operations++;
                if (logicBoard[x][y]) {
                    count++;
                }
            }
            if (count == 1) {
                System.out.println("Will be added for row: " + x);
                willBeAdded = true;
            }
            if (count > 1) {
                for (int y = 0; y < 9; y++) {
                    Logic.operations++;
                    if (logicBoard[x][y]) {
                        logicBoard[x][y] = false;
                    }
                }
            }
        }
        return logicBoard;
    }

    public void findMissingNumbersInRow(int[][] sudoku, int x) {
        List<Integer> emptyNumbersList = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));
        List<Integer> existingList = new ArrayList<Integer>();
        List<Integer> emptyIndexList = new ArrayList<Integer>();

        for (int y = 0; y < 9; y++) {
            Logic.operations++;
            System.out.print(sudoku[x][y] + "|");
            if (sudoku[x][y] != 0) {
                existingList.add(sudoku[x][y]);
            } else {
                emptyIndexList.add(y);
            }
        }
        emptyNumbersList.removeAll(existingList);
        System.out.print("\nMissing numbers: ");
        for (int l : emptyNumbersList) {

            System.out.print(l + "|");
        }
        System.out.println("\n");
        List<Integer> fitNumbersList;
        for (int y : emptyIndexList) {
            fitNumbersList = new ArrayList<Integer>();
            for (int n : emptyNumbersList) {
                boolean isColumnOk = findDuplicatesInColumn(sudoku, y, n);
                boolean isSquareOk = findDuplicatesInSquare(sudoku, x, y, n);
                Logic.operations++;
//                System.out.print("Number " + n + " in column " + y + " is ok: " + (isColumnOk&&isSquareOk) + "\n");
                if (isColumnOk&&isSquareOk) {
                    fitNumbersList.add(n);
                }
            }
            if (fitNumbersList.size() == 1) {
                System.out.println("Number " + fitNumbersList.get(0) + " will be added to: " + x + "x" + y);
                sudoku[x][y] = fitNumbersList.get(0);
                this.numberCounter[fitNumbersList.get(0)-1]++;
            }
            System.out.println("For column " + y + " fit numbers: " + fitNumbersList.size());
        }

    }

    public void findMissingNumbersInColumn(int[][] sudoku, int y) {
        List<Integer> emptyNumbersList = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));
        List<Integer> existingList = new ArrayList<Integer>();
        List<Integer> emptyIndexList = new ArrayList<Integer>();

        for (int x = 0; x < 9; x++) {
            Logic.operations++;
            System.out.print(sudoku[x][y] + "|");
            if (sudoku[x][y] != 0) {
                existingList.add(sudoku[x][y]);
            } else {
                emptyIndexList.add(x);
            }
        }
        emptyNumbersList.removeAll(existingList);
        System.out.print("\nMissing numbers: ");
        for (int l : emptyNumbersList) {

            System.out.print(l + "|");
        }
        System.out.println("\n");
        List<Integer> fitNumbersList;
        for (int x : emptyIndexList) {
            fitNumbersList = new ArrayList<Integer>();
            for (int n : emptyNumbersList) {
                boolean isRowOk = findDuplicatesInRow(sudoku, x, n);
                boolean isSquareOk = findDuplicatesInSquare(sudoku, x, y, n);
                Logic.operations++;
//                System.out.print("Number " + n + " in column " + y + " is ok: " + (isColumnOk&&isSquareOk) + "\n");
                if (isRowOk&&isSquareOk) {
                    fitNumbersList.add(n);
                }
            }
            if (fitNumbersList.size() == 1) {
                System.out.println("Number " + fitNumbersList.get(0) + " will be added to: " + x + "x" + y);
                sudoku[x][y] = fitNumbersList.get(0);
                this.numberCounter[fitNumbersList.get(0)-1]++;
            }
            System.out.println("For row " + x + " fit numbers: " + fitNumbersList.size());
        }

    }

    public boolean findDuplicatesInColumn(int[][] sudoku, int y, int n) {
        for (int x = 0; x < 9; x++) {
            Logic.operations++;
            if (sudoku[x][y] == n) {
                return false;
            }
        }
        return true;
    }

    public boolean findDuplicatesInRow(int[][] sudoku, int x, int n) {
        for (int y = 0; y < 9; y++) {
            Logic.operations++;
            if (sudoku[x][y] == n) {
                return false;
            }
        }
        return true;
    }

    public boolean findDuplicatesInSquare(int[][] sudoku, int x, int y, int n) {
        int counter = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                Logic.operations++;
                int k = 3 * (x / 3) + i;
                int l = 3 * (y / 3) + j;
                if (sudoku[k][l] == n) {
                    return false;
                }
            }
        }
        return true;
    }

}
