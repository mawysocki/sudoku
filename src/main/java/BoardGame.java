import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class BoardGame extends JPanel {

    public static final int SIZE = 100;
    private Font font;
    private JTextField[][] fields;
    private final BoardBorder boardBorder;

    public BoardGame() {
        this.setLayout(null);
        boardBorder = new BoardBorder();
        addFields();
        addStarterButton();
        new MockUpCreator(this);
    }


    private void addStarterButton() {
        JButton starter = new JButton();
        starter.setFont(font);
        starter.setText("Run");
        starter.addActionListener(this::clickStart);
        starter.setBounds(10 * SIZE, BoardBorder.OFFSET, 2 * SIZE, SIZE);
        this.add(starter);
    }

    public void paintComponent(Graphics g) {
        boardBorder.paintBorder(g);
    }

    private JTextField createTextFields(int x, int y) {
        JTextField text = new JTextField();
        int offsetX = boardBorder.getOffset(x), offsetY = boardBorder.getOffset(y);
        text.setBounds(x * SIZE + offsetX, y * SIZE + offsetY, SIZE, SIZE);
        text.setFont(font);
        text.setHorizontalAlignment(SwingConstants.CENTER);
        text.getDocument().addDocumentListener(new MyDocumentListener(text));
        return text;
    }

    private void addFields() {
        font = new Font("SansSerif", Font.BOLD, 50);
        fields = new JTextField[9][9];
        for (int j = 0; j < 9; j++) {
            for (int i = 0; i < 9; i++) {
                fields[i][j] = createTextFields(i, j);
                this.add(fields[i][j]);
            }
        }
    }


    public void clickStart(ActionEvent e) {
        int[][] array = initArray();
        SudokuResolver sudokuResolver = new SudokuResolver();
        sudokuResolver.calculate(array);
        updateBoard(sudokuResolver.getSolvedSudoku(), true);

    }

    private int[][] initArray() {
        int[][] sudoku = new int[9][9];
        for (int j = 0; j < 9; j++) {
            for (int i = 0; i < 9; i++) {
                String text = fields[i][j].getText();
                if (!text.equals("")) {
                    sudoku[i][j] = Integer.parseInt(fields[i][j].getText());
                }

            }
        }
        return sudoku;
    }

    public void updateBoard(int[][] sudoku, boolean isResolved) {
        for (int j = 0; j < 9; j++) {
            for (int i = 0; i < 9; i++) {
                if (isResolved) {
                    if (fields[i][j].getText().equals("") && sudoku[i][j] != 0) {
                        fields[i][j].setForeground(Color.GREEN);
                        fields[i][j].setText(String.valueOf(sudoku[i][j]));
                    }
                } else {
                    if (!fields[i][j].getText().equals("")) {
                        fields[i][j].setText("");
                        fields[i][j].setForeground(Color.BLACK);
                    }
                    if (sudoku[i][j] != 0) {
                        fields[i][j].setText(String.valueOf(sudoku[i][j]));
                    }

                }
            }
        }
    }
}
