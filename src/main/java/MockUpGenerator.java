import levels.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MockUpGenerator implements ActionListener {
    BoardGame boardGame;
    JComboBox levels;

    public MockUpGenerator(BoardGame bg, JComboBox levels) {
        this.boardGame = bg;
        this.levels = levels;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        MockUp mockUp = getMockUp((Level) levels.getSelectedItem());
        boardGame.updateBoard(mockUp.generateSudoku(), false);
    }

    public MockUp getMockUp(Level selectedLevel) {
        switch (selectedLevel) {
            case EASY:
                return new EasyLevel();
            case MEDIUM:
                return new MediumLevel();
            case HARD:
                return new HardLevel();
            case EXPERT:
                return new ExpertLevel();
            case IMPOSSIBLE:
                return new ImpossibleLevel();
            default:
                return null;
        }
    }


}
