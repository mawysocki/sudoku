package levels;

public class HardLevel implements MockUp{
    @Override
    public int[][] generateSudoku() {
        int[][] sudokuMockup = new int[9][9];
        sudokuMockup[7][0] = 6;

        sudokuMockup[3][1] = 6;
        sudokuMockup[4][1] = 8;
        sudokuMockup[5][1] = 5;
        sudokuMockup[6][1] = 4;

        sudokuMockup[0][2] = 7;
        sudokuMockup[1][2] = 6;
        sudokuMockup[2][2] = 4;
        sudokuMockup[4][2] = 1;
        sudokuMockup[6][2] = 5;

        sudokuMockup[0][3] = 4;
        sudokuMockup[3][3] = 5;
        sudokuMockup[7][3] = 1;

        sudokuMockup[0][4] = 6;
        sudokuMockup[1][4] = 8;
        sudokuMockup[7][4] = 5;

        sudokuMockup[5][5] = 2;
        sudokuMockup[8][5] = 7;

        sudokuMockup[2][6] = 1;
        sudokuMockup[4][6] = 2;
        sudokuMockup[5][6] = 6;
        sudokuMockup[6][6] = 3;

        sudokuMockup[3][7] = 8;
        sudokuMockup[4][7] = 5;
        sudokuMockup[5][7] = 1;
        sudokuMockup[8][7] = 6;

        sudokuMockup[1][8] = 5;
        sudokuMockup[3][8] = 4;
        return sudokuMockup;
    }
}
