package levels;

public class EasyLevel implements MockUp{
    @Override
    public int[][] generateSudoku() {
        int[][] sudokuMockup = new int[9][9];
        sudokuMockup[0][0] = 8;
        sudokuMockup[3][0] = 4;
        sudokuMockup[4][0] = 7;
        sudokuMockup[5][0] = 3;

        sudokuMockup[1][1] = 2;
        sudokuMockup[2][1] = 6;
        sudokuMockup[3][1] = 8;
        sudokuMockup[4][1] = 5;
        sudokuMockup[5][1] = 1;
        sudokuMockup[7][1] = 9;

        sudokuMockup[2][2] = 5;
        sudokuMockup[6][2] = 8;

        sudokuMockup[1][3] = 1;
        sudokuMockup[2][3] = 3;
        sudokuMockup[5][3] = 8;
        sudokuMockup[6][3] = 4;

        sudokuMockup[0][4] = 6;
        sudokuMockup[2][4] = 7;
        sudokuMockup[3][4] = 3;
        sudokuMockup[5][4] = 2;
        sudokuMockup[6][4] = 9;

        sudokuMockup[1][5] = 5;
        sudokuMockup[4][5] = 9;
        sudokuMockup[5][5] = 7;
        sudokuMockup[6][5] = 6;
        sudokuMockup[8][5] = 8;

        sudokuMockup[1][6] = 6;
        sudokuMockup[2][6] = 2;
        sudokuMockup[3][6] = 7;
        sudokuMockup[4][6] = 3;
        sudokuMockup[6][6] = 5;

        sudokuMockup[1][7] = 3;
        sudokuMockup[3][7] = 2;
        sudokuMockup[6][7] = 7;
        sudokuMockup[8][7] = 6;

        sudokuMockup[0][8] = 4;
        sudokuMockup[3][8] = 6;
        sudokuMockup[7][8] = 2;
        return sudokuMockup;
    }
}
