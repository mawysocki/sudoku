package levels;

public class MediumLevel implements MockUp {
    @Override
    public int[][] generateSudoku() {
        int[][] sudokuMockup = new int[9][9];
        sudokuMockup[0][0] = 9;
        sudokuMockup[1][0] = 2;
        sudokuMockup[3][0] = 4;
        sudokuMockup[4][0] = 5;

        sudokuMockup[4][1] = 7;
        sudokuMockup[5][1] = 9;
        sudokuMockup[8][1] = 5;

        sudokuMockup[0][2] = 4;
        sudokuMockup[2][2] = 5;
        sudokuMockup[6][2] = 3;
        sudokuMockup[8][2] = 9;

        sudokuMockup[3][3] = 7;
        sudokuMockup[6][3] = 6;
        sudokuMockup[7][3] = 8;

        sudokuMockup[3][4] = 6;

        sudokuMockup[0][5] = 6;
        sudokuMockup[1][5] = 8;
        sudokuMockup[2][5] = 7;
        sudokuMockup[3][5] = 5;
        sudokuMockup[4][5] = 3;
        sudokuMockup[5][5] = 4;

        sudokuMockup[0][6] = 2;
        sudokuMockup[8][6] = 6;

        sudokuMockup[1][7] = 6;
        sudokuMockup[2][7] = 1;
        sudokuMockup[3][7] = 9;
        sudokuMockup[5][7] = 5;
        sudokuMockup[8][7] = 8;

        sudokuMockup[0][8] = 5;
        sudokuMockup[1][8] = 3;
        sudokuMockup[4][8] = 6;
        sudokuMockup[6][8] = 7;
        return sudokuMockup;
    }
}

