package levels;

public class ExpertLevel implements MockUp{
    @Override
    public int[][] generateSudoku() {
        int[][] sudokuMockup = new int[9][9];
        sudokuMockup[0][0] = 8;
        sudokuMockup[3][0] = 4;
        sudokuMockup[7][0] = 1;

        sudokuMockup[2][1] = 3;
        sudokuMockup[3][1] = 9;
        sudokuMockup[5][1] = 5;
        sudokuMockup[6][1] = 6;

        sudokuMockup[2][2] = 1;
        sudokuMockup[3][2] = 6;

        sudokuMockup[0][3] = 4;
        sudokuMockup[3][3] = 5;
        sudokuMockup[7][3] = 6;

        sudokuMockup[1][4] = 1;
        sudokuMockup[7][4] = 3;
        sudokuMockup[8][4] = 8;

        sudokuMockup[2][5] = 6;
        sudokuMockup[4][5] = 3;
        sudokuMockup[7][5] = 7;

        sudokuMockup[2][7] = 9;
        sudokuMockup[8][7] = 5;

        sudokuMockup[1][8] = 7;
        sudokuMockup[5][8] = 6;
        return sudokuMockup;
    }
}
