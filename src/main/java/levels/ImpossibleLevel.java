package levels;

public class ImpossibleLevel implements MockUp {
    @Override
    public int[][] generateSudoku() {
        int[][] sudokuMockup = new int[9][9];
        sudokuMockup[4][0] = 3;
        sudokuMockup[6][0] = 4;

        sudokuMockup[2][1] = 9;
        sudokuMockup[6][1] = 6;

        sudokuMockup[0][2] = 5;
        sudokuMockup[3][2] = 7;
        sudokuMockup[7][2] = 3;
        sudokuMockup[8][2] = 9;

        sudokuMockup[7][3] = 4;

        sudokuMockup[1][4] = 8;
        sudokuMockup[3][4] = 6;

        sudokuMockup[2][5] = 1;
        sudokuMockup[4][5] = 8;
        sudokuMockup[7][5] = 5;
        sudokuMockup[8][5] = 3;

        sudokuMockup[6][6] = 9;

        sudokuMockup[2][7] = 5;
        sudokuMockup[4][7] = 6;
        sudokuMockup[7][7] = 8;
        sudokuMockup[8][7] = 1;

        sudokuMockup[0][8] = 7;
        sudokuMockup[5][8] = 2;
        return sudokuMockup;
    }
}
