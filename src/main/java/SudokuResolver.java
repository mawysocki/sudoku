public class SudokuResolver {
    Logic logic;
    BoardManager board;
    public void calculate(int[][] sudoku) {
        logic = new Logic();
        board = new BoardManager(logic, sudoku);
//        logic.resetBoard();
        logic.findEmptyOnBoard(board.sudoku);
//        board.displayNumberCount();
        int iteration = 0;
        int run = 0;
        int currentSum = 0;
        logic.checkSumControl();

        while (run < 1) {
            System.out.println("RUN: " + run);
            currentSum = 0;
            for (int i = 0; i < 9; i++) {
                System.out.println("\nFIND MISSING NUMBERS IN ROW: " + i);
                logic.findMissingNumbersInRow(board.sudoku, i);
            }
            while (!logic.checkSumControl() && iteration < 10) {
                if (logic.checkSum != currentSum) {
                    for (int i = 1; i < 10; i++) {
                        board.runAlgorythms(i);
                    }
                    currentSum = logic.checkSum;
                }
                else {
                    break;
                }
                iteration++;
            }
            run ++;
        }



//        System.out.println("ROUND 2");



        for (int i = 0; i < 9; i++) {
            System.out.println("\nFIND MISSING NUMBERS IN ROW: " + i);
            logic.findMissingNumbersInRow(board.sudoku, i);
        }
        board.printBoard();
        logic.checkSumControl();
        System.out.println("\nIteration: " + iteration);
        System.out.println("All actions: " + Logic.operations);
    }

    public int[][] getSolvedSudoku() {
        return board.sudoku;
    }
}
