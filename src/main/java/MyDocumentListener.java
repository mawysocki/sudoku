import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class MyDocumentListener implements DocumentListener {

    private JTextField jTextField;
    public MyDocumentListener(JTextField txt) {
        this.jTextField = txt;
    }
    @Override
    public void insertUpdate(DocumentEvent e) {
        validateField();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        validateField();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        validateField();
    }

    //Validates if input value is digit
    //If not it reset value
    private void validateField() {
        Runnable doAssist = () -> {
            String text = jTextField.getText();
            try {
                int number = Integer.parseInt(text);
                if (number>9 || number == 0) {
                    jTextField.setText("");
                }
            } catch (NumberFormatException e) {
                jTextField.setText("");
            }
        };
        SwingUtilities.invokeLater(doAssist);


    }
}
