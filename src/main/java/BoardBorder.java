import java.awt.*;

public class BoardBorder {

    public static final int OFFSET = 5;
    public void paintBorder(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(OFFSET));
        paintFraming(g2);
        paintCross(g2);
    }

    private void paintFraming(Graphics2D g2) {
        g2.drawLine(OFFSET, OFFSET, 9 * BoardGame.SIZE-OFFSET, OFFSET);
        g2.drawLine(OFFSET, OFFSET, OFFSET, 9 * BoardGame.SIZE);
        g2.drawLine(OFFSET, 9 * BoardGame.SIZE, 9 * BoardGame.SIZE, 9 * BoardGame.SIZE);
        g2.drawLine(9 * BoardGame.SIZE, OFFSET, 9 * BoardGame.SIZE, 9 * BoardGame.SIZE);
    }

    private void paintCross(Graphics2D g2) {
        g2.drawLine(3*BoardGame.SIZE,OFFSET, 3*BoardGame.SIZE, 9 * BoardGame.SIZE-OFFSET);
        g2.drawLine(6*BoardGame.SIZE,OFFSET, 6*BoardGame.SIZE, 9 * BoardGame.SIZE-OFFSET);
        g2.drawLine(OFFSET,3*BoardGame.SIZE, 9*BoardGame.SIZE-OFFSET, 3* BoardGame.SIZE);
        g2.drawLine(OFFSET,6*BoardGame.SIZE, 9*BoardGame.SIZE-OFFSET, 6* BoardGame.SIZE);
    }

    public int getOffset(int z) {
        if (z % 3 == 0) {
            return BoardBorder.OFFSET;
        }
        return 0;
    }
}
