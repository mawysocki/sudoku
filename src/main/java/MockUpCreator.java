import javax.swing.*;
import java.awt.*;

public class MockUpCreator{

    BoardGame boardGame;
    MockUpGenerator mockUpGenerator;

    JComboBox<Level> levels;

    public MockUpCreator(BoardGame boardGame) {
        this.boardGame = boardGame;
        createComboBox();
        createLabel();
    }

    private void createLabel() {
        JLabel label = new JLabel();
        label.setFont(new Font("SansSerif", Font.ITALIC, 25));
        label.setText("Mock level");
        label.setBounds(10 * BoardGame.SIZE, 3 * BoardGame.SIZE-BoardGame.SIZE/2, 2*BoardGame.SIZE, BoardGame.SIZE/2);
        boardGame.add(label);
    }
    private void createComboBox() {
        levels = new JComboBox<>();
        mockUpGenerator = new MockUpGenerator(this.boardGame, levels);
        levels.setFont(new Font("SansSerif", Font.ITALIC, 25));
        levels.setBounds(10 * BoardGame.SIZE, 3 * BoardGame.SIZE, 2 * BoardGame.SIZE, BoardGame.SIZE/2);
        addItems();
        levels.addActionListener(mockUpGenerator);
        boardGame.add(levels);
    }

    private void addItems() {
        levels.addItem(Level.EASY);
        levels.addItem(Level.MEDIUM);
        levels.addItem(Level.HARD);
        levels.addItem(Level.EXPERT);
        levels.addItem(Level.IMPOSSIBLE);
        levels.setSelectedIndex(-1);
    }


}
